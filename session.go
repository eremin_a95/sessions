package sessions

import (
	"sync"
	"time"
)

// Session is type for handling sessions
type Session struct {
	updated  time.Time
	token    string
	mutex    sync.Mutex
	lifetime time.Duration
	content  interface{}
}

func (s *Session) setLifetime(lifetime time.Duration) {
	s.mutex.Lock()
	s.lifetime = lifetime
	s.mutex.Unlock()
}

// Token returns string-token of Session
func (s *Session) Token() string {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	return s.token
}

// Expired checks if session was expired
func (s *Session) Expired() bool {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	return time.Since(s.updated) > s.lifetime
}

// Touch sets Session.created to time.Now()
func (s *Session) Touch() {
	s.mutex.Lock()
	s.updated = time.Now()
	s.mutex.Unlock()
}

// Content returns content of session as interface{}
func (s *Session) Content() interface{} {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	return s.content
}

// SetContent sets content of session to v
func (s *Session) SetContent(v interface{}) {
	s.mutex.Lock()
	s.content = v
	s.mutex.Unlock()
}
