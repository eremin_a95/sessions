# sessions

[![GoDoc](https://godoc.org/gitlab.com/eremin_a95/sessions?status.svg)](https://godoc.org/gitlab.com/eremin_a95/sessions) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/eremin_a95/sessions)](https://goreportcard.com/report/gitlab.com/eremin_a95/sessions)

---

`import "gitlab.com/eremin_a95/sessions"`

sessions is a package to handle sessions in your application.

You can create pools of sessions, create new sessions, get and delete sessions from pool by unique randomly-generated string-tokens.
Every session has a lifetime. If the session exists longer than the specified lifetime, it is considered expired. You can refresh session's lifetime by "touching" it.
New sessions creates according incoming args ...interface{}. Created session identifies by token - hex-string with defined length.

## Usage

First, you need to create session pool, with defined lifetime for each session in pool.
Then you can set functions for creating, deleting and token-generating functions

For example:
```go
newSession := func(args ...interface{}) (*sessions.Session, error) {
	if len(args) != 2 {
		return nil, fmt.Errorf("Error while creating session: Wrong number of arguments: %d, must be 2 (login, password)", len(args))
	}
	login, ok := args[0].(string)
	if !ok {
		return nil, fmt.Errorf("Error while creating session: First argument (login) must be string, but it is %T", args[0])
	}
	password, ok := args[1].(string)
	if !ok {
		return nil, fmt.Errorf("Error while creating session: Second argument (password) must be string, but it is %T", args[1])
	}
	if login != "login" || password != "password" {
		return nil, fmt.Errorf("Error while creating session: Incorrect login or password")
	}
	var session sessions.Session
	session.SetContent("You can put some object istead of string here. Or you can left session with <nil> content")
	return &session, nil
}

pool := sessions.NewPool(time.Hour)
pool.SetCreator(newSession)
defer pool.Close()
```
Here we define function *newSession* where we check *args* (we expect 2 arguments: *login* and *password*), return *error* if it is incorrect, and if everything is OK we create new session with string as content.
Then we create `Pool` with *time.Hour* as lifetime for sessions in pool and set *newSession* as function to create new sessions.
Functions to delete session and generate session-token set to default: deleter is empty function, generator is function which generates hex-string with length of *16* (actual "power" of token would be *8 bytes*).

In the next example, we are trying to create new session:
```go
session, err := pool.NewSession("login", "password")
if err != nil {
	panic(err)
}
fmt.Printf("Created session with token %s and content %s", session.Token(), session.Content())
```

In the bottom line we can use type assertion:
```go
fmt.Printf("Created session with token %s and content %s", session.Token(), session.Content().(string))
```

To get session from the pool use:
```go
session, err := pool.Session("0f1e2d3c4b5a6978")
if err != nil {
	panic(err)
}
fmt.Printf("Get session by token %s with content %s", session.Token(), session.Content())
```

You can check, if session was expired, and refresh its lifetime with `Touch()` method:
```go
if session.Expired() {
	fmt.Printf("Session with token %s was expired!", session.Token())
} else {
session.Touch()
}
```

Finnaly, if you want to delete session from pool, use:
```go
pool.Delete(session.Token())
```

### Example

Lets say we need to create temporary files in directory *files* for our web-server.
We have handler that responses with link to temporary file.
We don't want to create new file every time handler was executed.
Also we want to remove temporary files if they are not in use for more than 5 minutes.

We can use sessions to solve our problem.

Lets define type to store information about temporary file: filename and hash
```go
type TempFile struct {
	Filename string
	Hash	 string
}
```

Now lets define creator for our Pool:
```go
var createTempFile = func(args ...interface{}) (*sessions.Session, error) {
	if len(args) != 1 {
		return nil, fmt.Errorf("There must be one byte slice in args")
	}
	content, ok := arg.([]byte);
	if !ok {
		return nil, fmt.Errorf("Can't assert %T to []byte", content)
	}
	hash := sha256.Sum256(content)
	strHash := hex.EncodeToString(hash[:]
	var tempFile = &TempFile{
		Filename: "files/" + strHash,
		Hash:	  strHash,
	}
	file, ferr := os.Create(tempFile.Filename)
	if ferr != nil {
		return nil, fmt.Errorf("Error while creating file: %s", ferr)
	}
	if _, err := file.Write(content); err != nil {
		file.Close()
		os.Remove(rootDir + filename)
		return nil, fmt.Errorf("Error while writing to file: %s", err)
	}
	file.Close()
	session := new(sessions.Session)
	session.SetContent(tempFile)
	return session, nil
}
```

When session is expired we want to erase temporary file, so lets define deleter:
```go
var deleteTempFile = func(s *sessions.Session) {
	os.Remove(s.Content().(*TempFile).Filename)
}
```

To find out if there is already temporary file with such hash, we set token for each session as hash.
Lets define token generator:
```go
var tokenFromHash = func(s *sessions.Session) string {
	return s.Content().(*TempFile).Hash
}
```

Now we can create our pool:
```go
var tempFilesPool = sessions.NewPool(time.Minute * 5)
tempFilesPool.SetCreator(createTempFile)
tempFilesPool.SetDeleter(deleteTempFile)
tempFilesPool.SetTokenGenerator(tokenFromHash)
```

Now if we want to create new temporary file or use existing one and we need to return to client name for this file, we can use something like that:
```go
func createTempFile(content []byte) (string, error) {
	hash := sha256.Sum256(content)
	strHash := hex.EncodeToString(hash[:])
	if s, ok := tempFilesPool.Session(strHash); ok {
		s.Touch()
		return s.Content().(*TempFile).Filename, nil
	}
	s, err := tempFilesPool.NewSession(content)
	if err != nil {
		return "", nil
	}
	return s.Content().(*TempFile).Filename, nil
}
```
