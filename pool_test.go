package sessions

import (
	"fmt"
	"testing"
)

var newSession = func(args ...interface{}) (*Session, error) {
	if len(args) != 2 {
		return nil, fmt.Errorf("Error while creating session: Wrong number of arguments: %d, must be 2 (login, password)", len(args))
	}
	login, ok := args[0].(string)
	if !ok {
		return nil, fmt.Errorf("Error while creating session: First argument (login) must be string, but it is %T", args[0])
	}
	password, ok := args[1].(string)
	if !ok {
		return nil, fmt.Errorf("Error while creating session: Second argument (password) must be string, but it is %T", args[1])
	}
	if login != "login" || password != "password" {
		return nil, fmt.Errorf("Error while creating session: Incorrect login or password")
	}
	var session Session
	session.SetContent("Content")
	return &session, nil
}

var pool = NewPool(DefaultLifetime)

func init() {
	pool.SetCreator(newSession)
	pool.SetTokenGenerator(DefaultTokenGenerator(23))
}

func TestNewPool(t *testing.T) {
	if pool == nil {
		t.Error("Expected created pool, got nil")
	}
	if pool.size != 0 {
		t.Error("For", pool, "size expected", 0, "got", pool.size)
	}
	if len(pool.sessions) != 0 {
		t.Error("For", pool.sessions, "len expected", 0, "got", len(pool.sessions))
	}
}

func TestNewSession(t *testing.T) {
	startLen := len(pool.sessions)
	_, err := pool.NewSession()
	if err == nil {
		t.Error("For", err, "expected", "Error while creating session: Wrong number of arguments: 0, must be 2 (login, password)", "got", err)
	}
	_, err = pool.NewSession(5, 7.23)
	if err == nil {
		t.Error("For", err, "expected", "Error while creating session: First argument (login) must be string, but it is int", "got", err)
	}
	_, err = pool.NewSession("5", 7.23)
	if err == nil {
		t.Error("For", err, "expected", "Error while creating session: Second argument (password) must be string, but it is float", "got", err)
	}
	_, err = pool.NewSession("5", "7.23")
	if err == nil {
		t.Error("For", err, "expected", "Error while creating session: Incorrect login or password", "got", err)
	}
	session, err := pool.NewSession("login", "password")
	if err != nil {
		t.Error("For", err, "expected", nil, "got", err)
	}
	if session.Content() != "Content" {
		t.Error("For", session, "content expected", "Content", "got", session.Content())
	}
	if len(session.Token()) != 23 {
		t.Error("For", session.Token(), "len expected", 23, "got", len(session.Token()))
	}
	pool.NewSession("login", "password")
	endLen := len(pool.sessions)
	if (endLen - startLen) != 2 {
		t.Error("For (endLen - startLen) expected", 2, "got", (endLen - startLen))
	}
}

func TestSession(t *testing.T) {
	nsession, err := pool.NewSession("login", "password")
	if err != nil {
		t.Error("For", err, "expected", nil, "got", err)
	}
	gsession, ok := pool.Session(nsession.Token())
	if !ok {
		t.Error("For", ok, "expected", true, "got", ok)
	}
	if gsession != nsession {
		t.Error("For", gsession, "expected", nsession, "got", gsession)
	}
}

func TestDelete(t *testing.T) {
	session, err := pool.NewSession("login", "password")
	if err != nil {
		t.Error("For", err, "expected", nil, "got", err)
	}
	pool.Delete(session)
	nsession, ok := pool.Session(session.Token())
	if ok {
		t.Error("For", ok, "expected", false, "got", ok)
	}
	if nsession != nil {
		t.Error("For", nsession, "expected", nil, "got", nsession)
	}
}
