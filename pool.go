package sessions

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"sync"
	"time"
)

// DefaultLifetime is default lifetime for sessions (30 min)
const DefaultLifetime = 30 * time.Minute

type createFunc func(args ...interface{}) (*Session, error)

type deleteFunc func(*Session)

type tokenFunc func(*Session) string

// DefaultCreator is default session creator
// which just creates and returns new Session
var DefaultCreator = func(args ...interface{}) (*Session, error) { return new(Session), nil }

// DefaultDeleter is default empty session deleter
var DefaultDeleter = func(*Session) {}

// DefaultTokenGenerator returns token generation function
// which generate random hex string with tokenLen length
func DefaultTokenGenerator(tokenLen int) func(*Session) string {
	return func(*Session) string {
		buf := make([]byte, tokenLen/2+1)
		_, err := rand.Read(buf)
		if err != nil {
			// We HAVE TO panic if rand.Read returns error
			panic(err)
		}
		return hex.EncodeToString(buf)[0:tokenLen]
	}
}

// Pool is type for handling pools of sessions
type Pool struct {
	creator   createFunc
	deleter   deleteFunc
	generator tokenFunc
	sessions  map[string](*Session)
	mutex     sync.RWMutex
	lifetime  time.Duration
	size      int
	opened    bool
}

// NewPool creates new session pool with.
// lifetime as lifetime for all sessions in pool.
func NewPool(lifetime time.Duration) *Pool {
	var pool Pool
	pool.creator = DefaultCreator
	pool.deleter = DefaultDeleter
	pool.generator = DefaultTokenGenerator(16)
	pool.size = 0
	pool.sessions = make(map[string](*Session))
	pool.lifetime = lifetime
	pool.opened = true
	go func(p *Pool) {
		for !p.IsClosed() {
			p.mutex.Lock()
			p.clean()
			sleep := p.lifetime
			p.mutex.Unlock()
			time.Sleep(sleep)
		}
	}(&pool)
	return &pool
}

// SetCreator sets session creator for Pool.
// creator must return pointer to Session struct or error if args was incorrect.
// By default Pool uses DefaultCreator which creates new empty instance of Session.
func (p *Pool) SetCreator(creator func(args ...interface{}) (*Session, error)) {
	p.mutex.Lock()
	p.creator = creator
	p.mutex.Unlock()
}

// SetDeleter sets session deleter for Pool.
// deleter should free all resources which was additionally allocated by creator function.
// By default Pool uses empty DefaultDeleter.
func (p *Pool) SetDeleter(deleter func(*Session)) {
	p.mutex.Lock()
	p.deleter = deleter
	p.mutex.Unlock()
}

// SetTokenGenerator sets session's token generator for Pool.
// token must generate string which will be set as token for new session.
// token function can generate token depending on session (i.e. as hash of session.Content()).
// If you want to generate random hex-string with 'n' length you can get
// such function by calling DefaultTokenGenerator(n).
// By default Pool uses DefaultTokenGenerator(16).
func (p *Pool) SetTokenGenerator(generator func(*Session) string) {
	p.mutex.Lock()
	p.generator = generator
	p.mutex.Unlock()
}

// Pool must be locked while clean is executing
func (p *Pool) clean() {
	for _, s := range p.sessions {
		if s.Expired() {
			p.removeSession(s)
		}
	}
}

// Pool must be locked while removeSession is executing
func (p *Pool) removeSession(s *Session) {
	p.deleter(s)
	delete(p.sessions, s.Token())
}

// NewSession trying to create new session in pool with incoming args
func (p *Pool) NewSession(args ...interface{}) (*Session, error) {
	p.mutex.RLock()
	if !p.opened {
		p.mutex.RUnlock()
		return nil, fmt.Errorf("Error while creating session: Pool is closed")
	}
	p.mutex.RUnlock()
	session, err := p.creator(args...)
	if err != nil {
		return nil, err
	}
	p.mutex.Lock()
	defer p.mutex.Unlock()
	// Trying to generate token that is not presented in pool yet
	for ok := true; ok; {
		session.token = p.generator(session)
		var s *Session
		s, ok = p.sessions[session.token]
		if ok {
			if s.Expired() {
				// If such token is presented in pool and
				// session with this token is expired
				// we remove this session from pool
				p.removeSession(s)
				ok = false
			}
		}
	}
	session.lifetime = p.lifetime
	session.updated = time.Now()
	p.sessions[session.token] = session
	go func(pool *Pool) {
		p.mutex.Lock()
		// If number of sessions is less than Pool.size
		// then we decrease Pool.size
		// Else
		// If number of sessions reaches double Pool.size
		// we remove from pool all expired sessions and
		// set new size of map to Pool.size
		if len(pool.sessions) < pool.size {
			pool.size = len(pool.sessions)
		} else if len(pool.sessions) >= pool.size*2 {
			p.clean()
			pool.size = len(pool.sessions)
		}
		p.mutex.Unlock()
	}(p)
	return session, nil
}

// Session returns session by token or (nil, false) if there is no such token in pool
func (p *Pool) Session(token string) (*Session, bool) {
	p.mutex.RLock()
	defer p.mutex.RUnlock()
	if !p.opened {
		return nil, false
	}
	session, ok := p.sessions[token]
	return session, ok
}

// Delete removes session with such token from pool
func (p *Pool) Delete(s *Session) {
	p.mutex.Lock()
	if p.opened {
		p.removeSession(s)
	}
	p.mutex.Unlock()
}

// Close closes Pool. Pool can not be used anymore.
// Use Pool.Close() to be sured that for each session was called
// session deleter
func (p *Pool) Close() {
	p.mutex.Lock()
	p.opened = false
	p.mutex.Unlock()
	go func() {
		for _, s := range p.sessions {
			p.removeSession(s)
		}
		p.sessions = nil
	}()
}

// IsClosed checks if Pool is closed
func (p *Pool) IsClosed() bool {
	p.mutex.RLock()
	defer p.mutex.RUnlock()
	return !p.opened
}
